package com.savethemouse.main;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Rectangle;
import java.awt.image.BufferedImage;

public class StaticObject extends GameObject{	
	
	private BufferedImage wall_image;
	private BufferedImage entry_image;
	private BufferedImage exit_image;
	private BufferedImage uparrow_image;
	private BufferedImage downarrow_image;
	private BufferedImage leftarrow_image;
	private BufferedImage rightarrow_image;
	
	public StaticObject(int x, int y, ID id) {
		
		super(x, y, id);
		
		SpriteSheet ss = new SpriteSheet(Game.sprite_sheet);
		wall_image = ss.grabImage(3, 3, 100, 100);
		entry_image = ss.grabImage(1, 3, 100, 100);
		exit_image = ss.grabImage(2, 1, 100, 100);
		uparrow_image = ss.grabImage(3, 2, 100, 100);
		downarrow_image = ss.grabImage(1, 2, 100, 100);
		leftarrow_image = ss.grabImage(2, 2, 100, 100);
		rightarrow_image = ss.grabImage(3, 1, 100, 100);
	}

	public void tick() {
		
	}

	public void render(Graphics g) {

		switch (id) {
		case Wall:
			g.drawImage(wall_image, x, y, null);
			break;
			
		case Entry:
			g.drawImage(entry_image, x, y, null);
			break;
			
		case Exit:
			g.drawImage(exit_image, x, y, null);
			break;
			
		case UpArrow:
			g.drawImage(uparrow_image, x, y, null);
			break;
			
		case DownArrow:
			g.drawImage(downarrow_image, x, y, null);
			break;
			
		case LeftArrow:
			g.drawImage(leftarrow_image, x, y, null);
			break;
			
		case RightArrow:
			g.drawImage(rightarrow_image, x, y, null);
			break;
			
		default:
			break;
		}
	}

	public Rectangle getBounds() {

		return new Rectangle(x, y, 100, 100);
	}
	
	public Rectangle getBoundsCenter() {

		return new Rectangle(x + 49, y + 49, 2, 2);
	}

}
