package com.savethemouse.main;

import java.awt.Canvas;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.image.BufferStrategy;
import java.awt.image.BufferedImage;
import java.util.Random;

public class Game extends Canvas implements Runnable{
	
	private static final long serialVersionUID = -5982985806972589520L;
	
	public static final int WIDTH = 1016, HEIGHT = 900;
	
	private Thread thread;
	private boolean running = false;
	
	private Controller controller;
	
	public static BufferedImage sprite_sheet;
	
	public Game() {
		
		// Load sprite sheet
		BufferedImageLoader loader = new BufferedImageLoader();
		sprite_sheet = loader.loadImage("/spritesheet.png");
		
		// Init the GUI
		new Window(WIDTH, HEIGHT, "SaveTheMouse", this);
		
		// Init the controller
		controller = new Controller();
		this.addKeyListener(new KeyInput(controller));
		this.addMouseListener(new MouseInput());
		
		// Add Cats
		controller.addObject(new Cat(200, 300, controller));
		controller.addObject(new Cat(500, 100, controller));
		
		// Add Walls
		for (int i = 0; i <= 900; i = i + 100) {
			controller.addObject(new StaticObject(i, 0, ID.Wall));
			controller.addObject(new StaticObject(i, 700, ID.Wall));
		}
		for (int i = 0; i <= 700; i = i + 100) {
			controller.addObject(new StaticObject(0, i, ID.Wall));
			controller.addObject(new StaticObject(900, i, ID.Wall));
		}
		
		controller.addObject(new StaticObject(400, 100, ID.Wall));
		controller.addObject(new StaticObject(200, 200, ID.Wall));
		controller.addObject(new StaticObject(600, 200, ID.Wall));
		controller.addObject(new StaticObject(600, 300, ID.Wall));
		controller.addObject(new StaticObject(700, 300, ID.Wall));
		controller.addObject(new StaticObject(300, 400, ID.Wall));
		controller.addObject(new StaticObject(400, 500, ID.Wall));
		controller.addObject(new StaticObject(500, 500, ID.Wall));
		controller.addObject(new StaticObject(700, 500, ID.Wall));
		controller.addObject(new StaticObject(200, 600, ID.Wall));
		
		// Add Entry and Exit
		controller.addObject(new StaticObject(100, 100, ID.Entry));
		controller.addObject(new StaticObject(800, 600, ID.Exit));
		
		// Add Arrows
		controller.addObject(new StaticObject(300, 300, ID.RightArrow));
		controller.addObject(new StaticObject(500, 400, ID.RightArrow));
	}
	
	public static int clamp(int var, int min, int max) { // Make sure that no object can leave the screen
		
		if (var >= max) 
			return var = max;
		else if (var <= min)
			return var = min;
		else
			return var;
	}
	
	public synchronized void start() {
		
		thread = new Thread(this);
		thread.start();
		running = true;
	}
		
	public synchronized void stop() {
		
		try {
			thread.join();
			running = false;
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	// Game loop
	public void run() {
		
		long lastTime = System.nanoTime();
		double amoutOfTicks = 60.0;
		double ns = 1000000000 / amoutOfTicks;
		double delta = 0;
		long timer = System.currentTimeMillis();
		while (running) {
			long now = System.nanoTime();
			delta += (now - lastTime) / ns;
			lastTime = now;
			while (delta >= 1) {
				tick();
				delta--;
			}
			if (running) {
				render();
			}
			if (System.currentTimeMillis() - timer > 1000) {
				timer += 1000;
			}
		}
		
		stop();
;	}
	
	private void tick() {
	
		controller.tick();
	}
	
	private void render() {
		BufferStrategy bs = this.getBufferStrategy();
		if (bs == null) {
			this.createBufferStrategy(3);
			return;
		}
		
		Graphics g = bs.getDrawGraphics();	
		
		g.setColor(Color.DARK_GRAY);
		g.fillRect(0, 0, WIDTH, HEIGHT);
		
		controller.render(g);
		
		g.dispose();	
		bs.show();
	}
	
	public static void main(String arg[]) {
		new Game();
	}

}
