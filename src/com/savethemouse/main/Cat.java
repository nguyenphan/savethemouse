package com.savethemouse.main;

import java.awt.Graphics;
import java.awt.Rectangle;
import java.awt.image.BufferedImage;

public class Cat extends GameObject{
	
	Controller controller;
	
	private BufferedImage cat_image;

	public Cat(int x, int y, Controller controller) {
		
		super(x, y, ID.Cat);
		velY = 1;
		this.controller = controller;
		
		SpriteSheet ss = new SpriteSheet(Game.sprite_sheet);
		cat_image = ss.grabImage(1, 1, 100, 100);
	}

	public void tick() {
		
		x += velX;
	 	y += velY;
		
		x = Game.clamp(x, 0, Game.WIDTH - 100);
		y = Game.clamp(y, 0, Game.HEIGHT - 100);
		
		collision();
	}

	public void render(Graphics g) {
		
		g.drawImage(cat_image, x, y, null);
	}

	public Rectangle getBounds() {

		return new Rectangle(x, y, 100, 100);
	}

	public void collision() {
		
		for (int i = 0; i < controller.object.size(); i++) {
			GameObject tempObject = controller.object.get(i);

			if (tempObject.id == ID.Wall) {
				if (getBounds().intersects(tempObject.getBounds())) {
					velX *= -1;
					velY *= -1;
					break;
				}
			}
		}
	}

	@Override
	public Rectangle getBoundsCenter() {
		// TODO Auto-generated method stub
		return null;
	}
}
