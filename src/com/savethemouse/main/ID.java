package com.savethemouse.main;

public enum ID {

	Cat(),
	Mouse(),
	Path(),
	Wall(),
	Entry(),
	Exit(),
	UpArrow(),
	DownArrow(),
	LeftArrow(),
	RightArrow();
}
