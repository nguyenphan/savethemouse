package com.savethemouse.main;

import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

public class KeyInput extends KeyAdapter{
	
	private Controller controller;
	
	public KeyInput(Controller controller) {
		this.controller = controller;
	}

	public void keyPressed(KeyEvent e) {
		
		int key = e.getKeyCode();
		
		if (key == KeyEvent.VK_SPACE) {
			controller.addObject(new Mouse(100, 100, controller));
		}
	}
	
public void keyReleased(KeyEvent e) {
		
		e.getKeyCode();
	}
}
