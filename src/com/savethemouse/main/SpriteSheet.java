package com.savethemouse.main;

import java.awt.image.BufferedImage;

public class SpriteSheet {

	private BufferedImage sprite;
	
	public SpriteSheet(BufferedImage ss) {
		
		this.sprite = ss;
	}
	
	public BufferedImage grabImage(int col, int row, int width, int height) {
		
		BufferedImage img = sprite.getSubimage((row * 101) - 100, (col * 101) - 100, width, height);
		return img;
	}
}
