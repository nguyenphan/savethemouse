package com.savethemouse.main;

import java.awt.Graphics;
import java.awt.Rectangle;
import java.awt.image.BufferedImage;

public class Mouse extends GameObject{
	
	Controller controller;
	
	private BufferedImage mouse_image;
	
	public Mouse(int x, int y, Controller controller) {
		
		super(x, y, ID.Mouse);
		velX = 1;
		this.controller = controller;
		
		SpriteSheet ss = new SpriteSheet(Game.sprite_sheet);
		mouse_image = ss.grabImage(2, 3, 100, 100);
	}

	public void tick() {
		x += velX;
		y += velY;
		
		x = Game.clamp(x, 0, Game.WIDTH - 100);
		y = Game.clamp(y, 0, Game.HEIGHT - 100);
		
		collision();
	}
	
	public void render(Graphics g) {
		
		g.drawImage(mouse_image, x, y, null);
	}

	public Rectangle getBounds() {

		return new Rectangle(x, y, 100, 100);
	}
	
	public Rectangle getBoundsCenter() {

		return new Rectangle(x + 49, y + 49, 2, 2);
	}

	public void collision() {

		for (int i = 0; i < controller.object.size(); i++) {
			GameObject tempObject = controller.object.get(i);

			if (tempObject.id == ID.Cat)
				if (getBoundsCenter().intersects(tempObject.getBounds())) {
					controller.removeObject(this);
				}
			
			if (tempObject.id == ID.Exit)
				if (getBoundsCenter().intersects(tempObject.getBoundsCenter())) {
					controller.removeObject(this);
					System.out.println("Escape!");
				}
			
			if (tempObject.id == ID.UpArrow)
				if (getBoundsCenter().intersects(tempObject.getBoundsCenter())) {
					x += 1;
					y += 1;
					velY = -1;
					velX = 0;
				}
				
			if (tempObject.id == ID.DownArrow)
				if (getBoundsCenter().intersects(tempObject.getBoundsCenter())) {
					x += 1;
					y += 1;
					velY = 1;
					velX = 0;
				}
			
			if (tempObject.id == ID.LeftArrow)
				if (getBoundsCenter().intersects(tempObject.getBoundsCenter())) {
					x += 1;
					y += 1;
					velX = -1;
					velY = 0;
				}
			
			if (tempObject.id == ID.RightArrow)
				if (getBoundsCenter().intersects(tempObject.getBoundsCenter())) {
					x += 1;
					y += 1;
					velX = 1;
					velY = 0;
				}
			
		
			if (tempObject.id == ID.Wall) {
				if (getBounds().intersects(tempObject.getBounds())) {
					if (velY == 1) {
						y -= 1;
						velY = 0;
						velX = -1;
						System.out.println("Bottom");
					}
					else if (velX == -1) {
						x += 1;
						velX = 0;
						velY = -1;
						System.out.println("Left");
					}
					else if (velY == -1) {
						y += 1;
						velY = 0;
						velX = 1;
						System.out.println("Up");
					}
					else {
						x -= 1;
						velX = 0;
						velY = 1;
						System.out.println("Right");
					}
				}
			}
		}
	}
}
